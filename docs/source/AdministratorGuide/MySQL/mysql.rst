==============================
CERN centralized MySQL service
==============================

This document contains all information needed to manage the MySQL DBs. The service is provided by CERN IT in the "DBoD" service.
General info can be found in this `documentation <https://information-technology.web.cern.ch/services/database-on-demand>`_.

-----------------------
Communication channels:
-----------------------

CERN IT ES `homepage <https://information-technology.web.cern.ch/services/database-on-demand>`_

1. Tickets: open a `snow ticket <https://cern.service-now.com/service-portal?id=service_element&name=database-on-demand>`_.
2. Mattermost channel: LHCb `specific channel <https://mattermost.web.cern.ch/it-dep/channels/lhcbdbod>`_ or `IT general channel <https://mattermost.web.cern.ch/it-dep/channels/it-db>`_ (used also for Oracle).

Login to the database
=====================

How-To in `lbDevOps doc <https://lbdevops.web.cern.ch/lbdevops/DIRACInfrastructure.html#mysql-databases>`_.
