# DM Overview
The `DM Overview` dashboard can be found in the [Shifter Overview](https://monit-grafana.cern.ch/d/CSLWIV-4z/dm-overview?orgId=46) folder of the LHCb grafana organisation.
Plots are organised by four 'rows' (`Downloads`, `Uploads`, `FTS Transfers` and `Removals`).

## Downloads
- DM Downloads, All sites, All SEs (AccountingDB)
- Failed DM Downloads by Channel (AccountingDB)
- Failed DM Downloads by Source (AccountingDB)
- Failed DM Downloads by Destination (AccountingDB)

## Uploads
- DM Uploads, All SEs (AccountingDB)
- Failed DM Uploads by Channel (AccountingDB)
- Failed DM Uploads by Source (AccountingDB)
- Failed DM Uploads by Destination (AccountingDB)

## FTS Transfers
- All FTS Transfers by FinalStatus (AccountingDB)
- Failed FTS Transfers by Channel (AccountingDB)
- Failed FTS Transfers by Source (AccountingDB)
- Failed FTS Transfers by Destination (AccountingDB)

## Removals
- Removals, All SEs (AccountingDB)
- Failed Removals by Channel (AccountingDB)
