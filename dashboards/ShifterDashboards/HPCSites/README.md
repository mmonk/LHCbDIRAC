# HPC Sites
The `HPC Sites` dashboard can be found in the [Shifter Overview](https://monit-grafana.cern.ch/d/YPADvV-Vz/hpc-sites?orgId=46) folder of the LHCb grafana organisation.

## HPC Jobs by Site
Using AccountingDB as datasource

## HPC Jobs by Status
Using AccountingDB as datasource

## Running HPC Jobs by Site
Using WMS indices as datasource
.
Note: Not yet validated ([issue](https://github.com/DIRACGrid/WebAppDIRAC/issues/722) blocking validation)

## Failed HPC Jobs by Site
Using AccountingDB as datasource
