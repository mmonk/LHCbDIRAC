# Job Debugging
The `Job Debugging` dashboard can be found in the [Shifter Overview](https://monit-grafana.cern.ch/d/cQt42uAVk/job-debugging) folder of the LHCb grafana organisation.

## FinalMinorStatus
- Failed Jobs by FinalMinorStatus (AccountingDB)
- Completed Jobs by FinalMinorStatus (AccountingDB)
- Rescheduled Jobs by FinalMinorStatus (AccountingDB)

## Site
- Failed Jobs by Site (AccountingDB)
- Completed Jobs by Site (AccountingDB)
- Rescheduled Jobs by Site (AccountingDB)

## Job Group
- Failed Jobs by JobGroup (AccountingDB)
- Completed Jobs by JobGroup (AccountingDB)
- Rescheduled Jobs by JobGroup (AccountingDB)

## Job Type
- Failed Jobs by JobType (AccountingDB)
- Completed Jobs by JobType (AccountingDB)
- Rescheduled Jobs by JobType (AccountingDB)

## User
- Failed Jobs by User (AccountingDB)
- Completed Jobs by User (AccountingDB)
- Rescheduled Jobs by User (AccountingDB)
