# Overview
The `Overview` dashboard can be found in the [Shifter Overview](https://monit-grafana.cern.ch/d/qQoqDUA4z/overview?orgId=46) folder of the LHCb grafana organisation.

## Production Jobs by FinalMinorStatus
Using AccountingDB as datasource

## User Jobs by FinalMinorStatus
Using AccountingDB as datasource

## Job CPU Efficiency by JobType
Using AccountingDB as datasource

There are currently two types of this plot
- One using the `heatmap` visualisation which has a color legend but no way of adapting the color thresholds
- One using the `status history` visualisation which has no legend but the color thresholds can be chosen manually

## Jobs by JobSplitType
Using WMS indices as datasource

Note: Not yet validated ([issue](https://github.com/DIRACGrid/WebAppDIRAC/issues/722) blocking validation)

## Failed Transfers by Protocol
Using AccountingDB as datasource

## Pilots by Status
Using AccountingDB as datasource
