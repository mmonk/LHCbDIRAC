{
  "annotations": {
    "list": [
      {
        "builtIn": 1,
        "datasource": {
          "type": "grafana",
          "uid": "-- Grafana --"
        },
        "enable": true,
        "hide": true,
        "iconColor": "rgba(0, 211, 255, 1)",
        "name": "Annotations & Alerts",
        "target": {
          "limit": 100,
          "matchAny": false,
          "tags": [],
          "type": "dashboard"
        },
        "type": "dashboard"
      }
    ]
  },
  "description": "Replicate the DIRAC 'WMS Monitoring' functionality by querying from elasticsearch the WMS indices for given grouping keys and filters.",
  "editable": true,
  "fiscalYearStartMonth": 0,
  "graphTooltip": 0,
  "id": 5595,
  "links": [],
  "liveNow": false,
  "panels": [
    {
      "datasource": {
        "type": "influxdb",
        "uid": "000009465"
      },
      "gridPos": {
        "h": 4,
        "w": 24,
        "x": 0,
        "y": 0
      },
      "id": 5,
      "options": {
        "code": {
          "language": "plaintext",
          "showLineNumbers": false,
          "showMiniMap": false
        },
        "content": "\n\nThis dashboard lets users create plots based on the WMS monitoring data that is stored on ElasticSearch/OpenSearch.\n\nWhen querying over long time ranges, grafana aggregates (sums) `Metric` over larger time bins than the default ElasticSearch/OpenSearch ones (15m) to lower the number of data points\nit has to plot.\n\nTo account for this and show consistent values, `Metric` is calculated as the `Metric` per 15m, regardless of the actual bin size displayed",
        "mode": "markdown"
      },
      "pluginVersion": "10.1.5",
      "type": "text"
    },
    {
      "datasource": {
        "type": "elasticsearch",
        "uid": "iHcJcc-7z"
      },
      "description": "",
      "fieldConfig": {
        "defaults": {
          "color": {
            "mode": "palette-classic"
          },
          "custom": {
            "axisCenteredZero": false,
            "axisColorMode": "text",
            "axisLabel": "Jobs/15m",
            "axisPlacement": "auto",
            "barAlignment": 0,
            "drawStyle": "line",
            "fillOpacity": 20,
            "gradientMode": "none",
            "hideFrom": {
              "legend": false,
              "tooltip": false,
              "viz": false
            },
            "insertNulls": false,
            "lineInterpolation": "linear",
            "lineWidth": 1,
            "pointSize": 5,
            "scaleDistribution": {
              "type": "linear"
            },
            "showPoints": "auto",
            "spanNulls": false,
            "stacking": {
              "group": "A",
              "mode": "normal"
            },
            "thresholdsStyle": {
              "mode": "off"
            }
          },
          "mappings": [],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "green",
                "value": null
              },
              {
                "color": "red",
                "value": 80
              }
            ]
          },
          "unit": "si:"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 14,
        "w": 24,
        "x": 0,
        "y": 4
      },
      "id": 3,
      "options": {
        "legend": {
          "calcs": [
            "max",
            "mean",
            "min",
            "last"
          ],
          "displayMode": "table",
          "placement": "bottom",
          "showLegend": true
        },
        "tooltip": {
          "mode": "multi",
          "sort": "desc"
        }
      },
      "pluginVersion": "8.5.15",
      "targets": [
        {
          "alias": "",
          "bucketAggs": [
            {
              "field": "$ESGroupByKey",
              "id": "3",
              "settings": {
                "min_doc_count": "1",
                "order": "desc",
                "orderBy": "_count",
                "size": "0"
              },
              "type": "terms"
            },
            {
              "field": "timestamp",
              "id": "2",
              "settings": {
                "interval": "auto"
              },
              "type": "date_histogram"
            }
          ],
          "datasource": {
            "type": "elasticsearch",
            "uid": "iHcJcc-7z"
          },
          "metrics": [
            {
              "field": "$Metric",
              "id": "1",
              "settings": {
                "script": "_value * 3600 / $__interval_ms * 250"
              },
              "type": "sum"
            }
          ],
          "query": "ApplicationStatus: (${FilterByApplicationStatus:lucene}) AND JobGroup: (${FilterByJobGroup:lucene}) AND JobSplitType: (${FilterByJobSplitType:lucene}) AND MinorStatus: (${FilterByMinorStatus:lucene}) AND Site: (${FilterBySite:lucene}) AND Status: (${FilterByStatus:lucene}) AND User: (${FilterByUser:lucene}) AND UserGroup: (${FilterByUserGroup:lucene})",
          "refId": "A",
          "timeField": "timestamp"
        }
      ],
      "title": "Sum of $FilterByStatus $Metric Per Hour By $ESGroupByKey (ElasticSearch)",
      "transformations": [],
      "type": "timeseries"
    },
    {
      "datasource": {
        "type": "elasticsearch",
        "uid": "iHcJcc-7z"
      },
      "description": "",
      "fieldConfig": {
        "defaults": {
          "color": {
            "mode": "palette-classic"
          },
          "custom": {
            "axisCenteredZero": false,
            "axisColorMode": "text",
            "axisLabel": "Jobs/15m",
            "axisPlacement": "auto",
            "barAlignment": 0,
            "drawStyle": "line",
            "fillOpacity": 20,
            "gradientMode": "none",
            "hideFrom": {
              "legend": false,
              "tooltip": false,
              "viz": false
            },
            "insertNulls": false,
            "lineInterpolation": "linear",
            "lineWidth": 1,
            "pointSize": 5,
            "scaleDistribution": {
              "type": "linear"
            },
            "showPoints": "auto",
            "spanNulls": false,
            "stacking": {
              "group": "A",
              "mode": "normal"
            },
            "thresholdsStyle": {
              "mode": "off"
            }
          },
          "mappings": [],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "green",
                "value": null
              },
              {
                "color": "red",
                "value": 80
              }
            ]
          },
          "unit": "si:"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 14,
        "w": 24,
        "x": 0,
        "y": 18
      },
      "id": 6,
      "options": {
        "legend": {
          "calcs": [
            "max",
            "mean",
            "min",
            "last",
            "lastNotNull"
          ],
          "displayMode": "table",
          "placement": "bottom",
          "showLegend": true
        },
        "tooltip": {
          "mode": "multi",
          "sort": "desc"
        }
      },
      "pluginVersion": "8.5.15",
      "targets": [
        {
          "alias": "",
          "bucketAggs": [
            {
              "field": "$ESGroupByKey",
              "id": "3",
              "settings": {
                "min_doc_count": "1",
                "order": "desc",
                "orderBy": "_count",
                "size": "0"
              },
              "type": "terms"
            },
            {
              "field": "timestamp",
              "id": "2",
              "settings": {
                "interval": "auto"
              },
              "type": "date_histogram"
            }
          ],
          "datasource": {
            "type": "elasticsearch",
            "uid": "iHcJcc-7z"
          },
          "metrics": [
            {
              "field": "$Metric",
              "hide": false,
              "id": "1",
              "settings": {
                "script": "_value * 3600 / $__interval_ms * 250"
              },
              "type": "avg"
            }
          ],
          "query": "ApplicationStatus: (${FilterByApplicationStatus:lucene}) AND JobGroup: (${FilterByJobGroup:lucene}) AND JobSplitType: (${FilterByJobSplitType:lucene}) AND MinorStatus: (${FilterByMinorStatus:lucene}) AND Site: (${FilterBySite:lucene}) AND Status: (${FilterByStatus:lucene}) AND User: (${FilterByUser:lucene}) AND UserGroup: (${FilterByUserGroup:lucene})",
          "refId": "A",
          "timeField": "timestamp"
        }
      ],
      "title": "Average of $FilterByStatus $Metric Per Hour By $ESGroupByKey (ElasticSearch)",
      "transformations": [
        {
          "id": "renameByRegex",
          "options": {
            "regex": "metric ",
            "renamePattern": ""
          }
        }
      ],
      "type": "timeseries"
    }
  ],
  "refresh": "",
  "schemaVersion": 38,
  "style": "dark",
  "tags": [],
  "templating": {
    "list": [
      {
        "current": {
          "selected": true,
          "text": "Jobs",
          "value": "Jobs"
        },
        "hide": 0,
        "includeAll": false,
        "label": "Metric",
        "multi": false,
        "name": "Metric",
        "options": [
          {
            "selected": false,
            "text": "Reschedules",
            "value": "Reschedules"
          },
          {
            "selected": true,
            "text": "Jobs",
            "value": "Jobs"
          }
        ],
        "query": "Reschedules, Jobs",
        "queryValue": "",
        "skipUrlSync": false,
        "type": "custom"
      },
      {
        "allValue": "*",
        "current": {
          "selected": true,
          "text": [
            "All"
          ],
          "value": [
            "$__all"
          ]
        },
        "datasource": {
          "type": "elasticsearch",
          "uid": "iHcJcc-7z"
        },
        "definition": "{\"find\": \"terms\", \"field\": \"MinorStatus\"}",
        "hide": 0,
        "includeAll": true,
        "label": "MinorStatus Filter",
        "multi": true,
        "name": "FilterByMinorStatus",
        "options": [],
        "query": "{\"find\": \"terms\", \"field\": \"MinorStatus\"}",
        "refresh": 1,
        "regex": "",
        "skipUrlSync": false,
        "sort": 0,
        "type": "query"
      },
      {
        "allValue": "*",
        "current": {
          "selected": true,
          "text": [
            "All"
          ],
          "value": [
            "$__all"
          ]
        },
        "datasource": {
          "type": "elasticsearch",
          "uid": "iHcJcc-7z"
        },
        "definition": "{\"find\": \"terms\", \"field\": \"Site\"}",
        "hide": 0,
        "includeAll": true,
        "label": "Site Filter",
        "multi": true,
        "name": "FilterBySite",
        "options": [],
        "query": "{\"find\": \"terms\", \"field\": \"Site\"}",
        "refresh": 1,
        "regex": "",
        "skipUrlSync": false,
        "sort": 0,
        "type": "query"
      },
      {
        "allValue": "*",
        "current": {
          "selected": true,
          "text": [
            "Running"
          ],
          "value": [
            "Running"
          ]
        },
        "datasource": {
          "type": "elasticsearch",
          "uid": "iHcJcc-7z"
        },
        "definition": "{\"find\": \"terms\", \"field\": \"Status\"}",
        "hide": 0,
        "includeAll": true,
        "label": "Status Filter",
        "multi": true,
        "name": "FilterByStatus",
        "options": [],
        "query": "{\"find\": \"terms\", \"field\": \"Status\"}",
        "refresh": 1,
        "regex": "",
        "skipUrlSync": false,
        "sort": 0,
        "type": "query"
      },
      {
        "current": {
          "selected": false,
          "text": "JobSplitType",
          "value": "JobSplitType"
        },
        "datasource": {
          "type": "elasticsearch",
          "uid": "iHcJcc-7z"
        },
        "definition": "{\"find\": \"fields\"}",
        "hide": 0,
        "includeAll": false,
        "label": "ES GroupBy Key",
        "multi": false,
        "name": "ESGroupByKey",
        "options": [],
        "query": "{\"find\": \"fields\"}",
        "refresh": 1,
        "regex": "",
        "skipUrlSync": false,
        "sort": 0,
        "type": "query"
      },
      {
        "allValue": "*",
        "current": {
          "selected": true,
          "text": [
            "All"
          ],
          "value": [
            "$__all"
          ]
        },
        "datasource": {
          "type": "elasticsearch",
          "uid": "iHcJcc-7z"
        },
        "definition": "{\"find\": \"terms\", \"field\": \"ApplicationStatus\"}",
        "hide": 0,
        "includeAll": true,
        "label": "ApplicationStatus Filter",
        "multi": true,
        "name": "FilterByApplicationStatus",
        "options": [],
        "query": "{\"find\": \"terms\", \"field\": \"ApplicationStatus\"}",
        "refresh": 1,
        "regex": "",
        "skipUrlSync": false,
        "sort": 0,
        "type": "query"
      },
      {
        "allValue": "*",
        "current": {
          "selected": true,
          "text": [
            "All"
          ],
          "value": [
            "$__all"
          ]
        },
        "datasource": {
          "type": "elasticsearch",
          "uid": "iHcJcc-7z"
        },
        "definition": "{\"find\": \"terms\", \"field\": \"JobGroup\"}",
        "hide": 0,
        "includeAll": true,
        "label": "JobGroup Filter",
        "multi": true,
        "name": "FilterByJobGroup",
        "options": [],
        "query": "{\"find\": \"terms\", \"field\": \"JobGroup\"}",
        "refresh": 1,
        "regex": "",
        "skipUrlSync": false,
        "sort": 0,
        "type": "query"
      },
      {
        "allValue": "*",
        "current": {
          "selected": true,
          "text": [
            "All"
          ],
          "value": [
            "$__all"
          ]
        },
        "datasource": {
          "type": "elasticsearch",
          "uid": "iHcJcc-7z"
        },
        "definition": "{\"find\": \"terms\", \"field\": \"JobSplitType\"}",
        "hide": 0,
        "includeAll": true,
        "label": "JobSplitType Filter",
        "multi": true,
        "name": "FilterByJobSplitType",
        "options": [],
        "query": "{\"find\": \"terms\", \"field\": \"JobSplitType\"}",
        "refresh": 1,
        "regex": "",
        "skipUrlSync": false,
        "sort": 0,
        "type": "query"
      },
      {
        "allValue": "*",
        "current": {
          "selected": true,
          "text": [
            "All"
          ],
          "value": [
            "$__all"
          ]
        },
        "datasource": {
          "type": "elasticsearch",
          "uid": "iHcJcc-7z"
        },
        "definition": "{\"find\": \"terms\", \"field\": \"User\"}",
        "hide": 0,
        "includeAll": true,
        "label": "User Filter",
        "multi": true,
        "name": "FilterByUser",
        "options": [],
        "query": "{\"find\": \"terms\", \"field\": \"User\"}",
        "refresh": 1,
        "regex": "",
        "skipUrlSync": false,
        "sort": 0,
        "type": "query"
      },
      {
        "allValue": "*",
        "current": {
          "selected": true,
          "text": [
            "All"
          ],
          "value": [
            "$__all"
          ]
        },
        "datasource": {
          "type": "elasticsearch",
          "uid": "iHcJcc-7z"
        },
        "definition": "{\"find\": \"terms\", \"field\": \"UserGroup\"}",
        "hide": 0,
        "includeAll": true,
        "label": "UserGroup Filter",
        "multi": true,
        "name": "FilterByUserGroup",
        "options": [],
        "query": "{\"find\": \"terms\", \"field\": \"UserGroup\"}",
        "refresh": 1,
        "regex": "",
        "skipUrlSync": false,
        "sort": 0,
        "type": "query"
      }
    ]
  },
  "time": {
    "from": "now-24h",
    "to": "now"
  },
  "timepicker": {},
  "timezone": "",
  "title": "WMS Monitoring (ES)",
  "uid": "ZBG1U-yVz",
  "version": 29,
  "weekStart": ""
}
