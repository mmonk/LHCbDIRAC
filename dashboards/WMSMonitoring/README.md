# WMS Monitoring (OpenSearch)
This dashboard creates plots based on the WMS monitoring data stored on OpenSearch (index: lhcb-production_wmshistory_*).

Grafana aggregates (sums) data when querying for large time intervals to lower the amount of data points it has to plot. Because of this, the bin size and scale of the y-axis are dependent on the time interval chosen. This is countered by calculating the metric as metric per 15m: `metric = metric / BIN_SIZE_IN_S * 3600 / 4`.

The metrics can be filtered on the following fields:
- `MinorStatus`
- `Site`
- `Status`
- `ApplicationStatus`
- `JobGroup`
- `JobSplitType`
- `User`
- `UserGroup`

The metrics can technically be grouped on all the fields that exist in the index. In reality, some groupings may be harder than other (e.g. JobGroup) because of the high cardinality of the chosen grouping field.

## metrics
- Sum of jobs per hour by <grouping field>
- Average of jobs per hour by <grouping field>
