v10r2p18
Based on DIRAC v7r3p9
LHCbWebDIRAC v5r4
LHCbDIRACOS v1r23
v10r2p17
Based on DIRAC v7r3p9
LHCbWebDIRAC v5r4
LHCbDIRACOS v1r23
Thank you for writing the text to appear in the release notes. It will show up
exactly as it appears between the two bold lines
Please follow the template:

*DMScript

FIX: (!1122) fix exit code if `'Failed'` items in method result. To solve issue https://github.com/DIRACGrid/DIRAC/issues/5580

*TransformationDebug

FIX: (!1118) option `--Job` was creating an infinite DB query if no transformation ID was provided. This is now fixed
FIX: (!1118) problem in py3 with sorting None objects
CHANGE: (!1118) Use `defaultDict` whenever possible instead of `setdefault()`

*dirac-loop

CHANGE: (!1118) add option `--Path` to extract paths (BK paths or LFNs) from text

*ProductionManagementSystem

FIX: (!1116) NotifyAgent: only send mails if there's something to send

*Bookkeeping

FIX: (!1115) fixed construction of getFiles query



v10r2p16
Based on DIRAC v7r2p33
LHCbWebDIRAC v5r3p2
LHCbDIRACOS v1r23
*Tests

CHANGE: (!1109) test_setup_py3 uses DIRAC branch instead of release

*Worfklow

FIX: (!1108) being a bit more conservative (75% instead of 80%) when calculating how many events to produce

*DataManagementSystem

FIX: (!1107) ScanPopularity, do not exit after first attempt to contact a service
NEW: (!1099) infer the "Data Challenge" activity in the LHCbFTS3Plugin

*Interface

FIX: (!1106) avoid changing the dict while looping in DiracLHCb.bkQueryProduction

*CI

CHANGE: (!1102) allow to run tornado services with the docker container

*TransformationSystem

FIX: (!1098) MCSimulationTestingAgent: fix: getting the number of events produced from the Gauss job



v10r2p15
Based on DIRAC v7r2p30
LHCbWebDIRAC v5r3p1
LHCbDIRACOS v1r23
*Docs

FIX: (!1096) indent to 4 in createReleaseFiles



v10r2p14
Based on DIRAC v7r2p30
LHCbWebDIRAC v5r3p1
LHCbDIRACOS v1r23


v10r2p13
Based on DIRAC v7r2p28
LHCbWebDIRAC v5r3p1
LHCbDIRACOS v1r23
v10r2p12
Based on DIRAC v7r2p28
LHCbWebDIRAC v5r3p1
LHCbDIRACOS v1r22
v10r2p11
Based on DIRAC v7r2p25
LHCbWebDIRAC v5r3p1
LHCbDIRACOS v1r22
v10r2p10
Based on DIRAC v7r2p22
LHCbWebDIRAC v5r3p1
LHCbDIRACOS v1r22
*WorkloadManagement

CHANGE: (!1072) Remove the hack for migrating to a new sandbox store volume

*TransformationSystem

CHANGE: (!1071) define an option `OverflowSEs` in the `Operations/TransformationPlugin` section.
FIX: (!1064) Fix AttributeError in dirac-transformation-debug with Python 3

*Core

FIX: (!1070) Fix extracting GUID from ROOT files with Python 3

*Bookkeeping

FIX: (!1067) Fix running dirac-bookkeeping-genXMLCatalog with --Options

*ProductionManagementSystem

NEW: (!1061) ProductionStatusAgent avoids race condition when setting transformation status



v10r2p9
Based on DIRAC v7r2p19
LHCbWebDIRAC v5r3p1
LHCbDIRACOS v1r20
v10r2p8
Based on DIRAC v7r2p19
LHCbWebDIRAC v5r3p1
LHCbDIRACOS v1r21
*CI

FIX: (!1062) update apt cache before installing curl



v10r2p7
Based on DIRAC v7r2p17
LHCbWebDIRAC v5r3p1
LHCbDIRACOS v1r20
*Data Management

CHANGE: (!1056) Optimise running checkPhysicalFiles when it is guaranteed to fail due to storage elements being banned

*Production Management

FIX: (!1055) Only flush transformations if there are unused files

*Production

FIX: (!1053) Fix inverted if condition in ProductionStatusAgent that causes Idle transformations to be finished prematurely



v10r2p6
Based on DIRAC v7r2p16
LHCbWebDIRAC v5r3
LHCbDIRACOS v1r20


v10r2p5
Based on DIRAC v7r2p14
LHCbWebDIRAC v5r3
LHCbDIRACOS v1r20
*Production Management

NEW: (!1036) Handle Idle Analysis Production transformations in ProductionStatusAgent

*TransformationSystem

FIX: (!1033) correct order of import in dirac-test-script
FIX: (!1032) adding runs to a BK query containing a single run was not working, neither was the `--List` option in `dirac-production-set-runs`
FIX (`TransformationPlugin`): pb with integer division (urgent)
FIX: (!1029) Fix submitting files without a RunNumber (i.e. MCReconstruction)

*DataManagementSystem

FIX: (!1032) use integer division `//`

*Interfaces

CHANGE: (!1031) add specific LHCb Optimizer when needed

*Bookkeeping

FIX: (!1028) Fix TypeErrors from logging in XMLFilesReaderManager



v10r2p4
Based on DIRAC v7r2p13
LHCbWebDIRAC v5r3
LHCbDIRACOS v1r20
v10r2p3
Based on DIRAC v7r2p10
LHCbWebDIRAC v5r3
LHCbDIRACOS v1r20
*WorkloadManagement

CHANGE: (!1023) Add hacky version of SandboxStoreHandler for migrating to a new volume

*TransformationSystem

FIX: (!1020) Fix "Execution failed.: ( 1366: Incorrect integer value: 'None' for column 'RunNumber'"



v10r2p2
Based on DIRAC v7r2p9
LHCbWebDIRAC v5r3
LHCbDIRACOS v1r19
*Bookkeeping

FIX: (!1013) Fix dirac-bookkkeeping-CLI for Python 3
FIX: (!1013) Avoid crash when parsing invalid bookkeeping queries



v10r2p1
Based on DIRAC v7r2p8
LHCbWebDIRAC v5r3
LHCbDIRACOS v1r19
*ConfigurationSystem

CHANGE: (!1011) Update test references to use x86_64 microarchitecture levels



v10r2
Based on DIRAC v7r2p8
LHCbWebDIRAC  v5r3
LHCbDIRACOS v1r19

v10r2-pre13
Based on DIRAC v7r2p7
LHCbWebDIRAC  v5r3-pre2
LHCbDIRACOS v1r18
*Core

CHANGE: (!1008) Remove JSONPickle module



v10r2-pre12
Based on DIRAC v7r2p6
LHCbWebDIRAC v5r3-pre1
LHCbDIRACOS v1r18


v10r2-pre11
Based on DIRAC v7r2p6
LHCbWebDIRAC v5r3-pre1
LHCbDIRACOS v1r18
v10r2-pre10
Based on DIRAC v7r2p6
LHCbWebDIRAC v5r3-pre1
LHCbDIRACOS v1r18
v10r2-pre9
Based on DIRAC v7r2p6
LHCbWebDIRAC v5r3-pre1
LHCbDIRACOS v1r17


v10r2-pre8
Based on DIRAC v7r2p4
LHCbWebDIRAC v5r3-pre1
LHCbDIRACOS v1r17


v10r2-pre7
Based on DIRAC v7r2p4
LHCbWebDIRAC v5r3-pre1
LHCbDIRACOS v1r17
Thank you for writing the text to appear in the release notes. It will show up
exactly as it appears between the two bold lines
Please follow the template:

*Subsystem

NEW/CHANGE/FIX: (!969) explanation
For examples look into release.notes



v10r2-pre6
Based on DIRAC v7r2p2
LHCbWebDIRAC v5r2p1
LHCbDIRACOS v1r16


v10r2-pre5
Based on DIRAC v7r2p2
LHCbWebDIRAC v5r2p1
LHCbDIRACOS v1r16
v10r2-pre4
Based on DIRAC v7r2-pre38
LHCbWebDIRAC v5r2p1
LHCbDIRACOS v1r16


v10r2-pre3
Based on DIRAC v7r2-pre35
LHCbWebDIRAC v5r2p1
LHCbDIRACOS v1r16
v10r2-pre2
Based on DIRAC v7r2-pre33
LHCbWebDIRAC v5r2
LHCbDIRACOS v1r15

*tests

FIX: (!902) adjust pylint and pytest for new repo layout

*ConfigurationSystem

CHANGE: (!905) Get CA certificates from CVMFS when querying the software configuration database

*Python3

NEW: (!904) Move to src repository layout
NEW: (!904) Run unit tests with Python 3

*Python 3

CHANGE: (!910) Convert scripts to new entrypoint compatible style
NEW: (!911) Support installing a client with pip

v10r2-pre1
Based on DIRAC v7r2-pre33
LHCbWebDIRAC v5r2
LHCbDIRACOS v1r15



v10r2-pre1
Based on DIRAC v7r2-pre31
LHCbWebDIRAC v5r2
LHCbDIRACOS v1r13
Start of the v10r2 LHCbDIRAC series, based on DIRAC v7r2.
