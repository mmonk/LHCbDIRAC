###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""""Integration" production jobs. StepIDs are taken from REAL productions that ran "recently"."""


def test_Reco17Success(diracLHCb):
    from LHCbDIRAC.Interfaces.API.DiracProduction import DiracProduction
    from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequest import ProductionRequest

    lfns = ["/lhcb/data/2017/RAW/FULL/LHCb/COLLISION17/192165/192165_0000000011.raw"]
    # From request 39597
    optionFilesDQ = "$APPCONFIGOPTS/DaVinci/DVMonitor-RealData.py;"
    optionFilesDQ += "$APPCONFIGOPTS/DaVinci/DataType-2016.py;"
    optionFilesDQ += "$APPCONFIGOPTS/DaVinci/DaVinci-InputType-SDST.py"
    stepsInProd = [
        {
            "StepId": "131333",
            "StepName": "Reco17a",
            "ApplicationName": "Brunel",
            "ApplicationVersion": "v52r4",
            "ExtraPackages": "AppConfig.v3r323;Det/SQLDDDB.v7r11",
            "ProcessingPass": "Reco17a",
            "Visible": "Y",
            "Usable": "Yes",
            "DDDB": "dddb-20150724",
            "CONDDB": "cond-20170510",
            "DQTag": "",
            "OptionsFormat": "",
            "OptionFiles": "$APPCONFIGOPTS/Brunel/DataType-2017.py;$APPCONFIGOPTS/Brunel/rdst.py",
            "mcTCK": "",
            "ExtraOptions": "",
            "isMulticore": "N",
            "SystemConfig": "",
            "fileTypesIn": ["RAW"],
            "fileTypesOut": ["BRUNELHIST", "RDST"],
            "visibilityFlag": [{"Visible": "N", "FileType": "RDST"}, {"Visible": "Y", "FileType": "BRUNELHIST"}],
        },
        {
            "StepId": 131327,
            "StepName": "DataQuality-FULL",
            "ApplicationName": "DaVinci",
            "ApplicationVersion": "v42r4",
            "ExtraPackages": "AppConfig.v3r324;Det/SQLDDDB.v7r11",
            "ProcessingPass": "DataQuality-FULL",
            "Visible": "N",
            "Usable": "Yes",
            "DDDB": "dddb-20150724",
            "CONDDB": "cond-20170510",
            "DQTag": "",
            "OptionsFormat": "DQ",
            "OptionFiles": optionFilesDQ,
            "isMulticore": "N",
            "SystemConfig": "",
            "mcTCK": "",
            "ExtraOptions": "",
            "fileTypesIn": ["RDST"],
            "fileTypesOut": ["DAVINCIHIST"],
            "visibilityFlag": [{"Visible": "Y", "FileType": "DAVINCIHIST"}],
        },
    ]

    prod = ProductionRequest()._buildProduction(
        "Reconstruction",
        stepsInProd,
        {"RDST": "Tier1-Buffer"},
        0,
        100,
        outputMode="Run",
        inputDataPolicy="protocol",
        inputDataList=lfns,
        events=25,
    )
    prod.LHCbJob.setInputSandbox("pilot.cfg")
    prod.LHCbJob.setConfigArgs("pilot.cfg")
    prod.LHCbJob._addParameter(prod.LHCbJob.workflow, "runNumber", "JDL", 192165, "Input run number")
    res = DiracProduction().launchProduction(prod, False, True, 0)
    assert res["OK"], res


# def test_StrippSuccess(diracLHCb):
#     from LHCbDIRAC.Interfaces.API.DiracProduction import DiracProduction
#     from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequest import ProductionRequest

#     # FIXME: needs a new test with recent app

#     # lfns = ['']
#     # # ancestor: ''

#     # optionFiles = ''
#     # optionFiles += ''
#     # stepsInProd = [{'StepId': , 'StepName': '',
#     #                 'ApplicationName': 'DaVinci', 'ApplicationVersion': '',
#     #                 'ExtraPackages': '',
#     #                 'ProcessingPass': '', 'Visible': 'Y', 'Usable': 'Yes',
#     #                 'DDDB': '', 'CONDDB': '', 'DQTag': '', 'OptionsFormat': 'Stripping',
#     #                 'OptionFiles': optionFiles,
#     #                 'isMulticore': 'N', 'SystemConfig': '', 'mcTCK': '', 'ExtraOptions': '',
#     #                 'fileTypesIn': ['RDST'],
#     #                 'fileTypesOut':['BHADRON.MDST', 'BHADRONCOMPLETEEVENT.DST', 'CHARM.MDST',
#     #                                 'CHARMCOMPLETEEVENT.DST', 'DIMUON.DST', 'EW.DST', 'FTAG.DST', 'LEPTONIC.MDST',
#     #                                 'MINIBIAS.DST', 'RADIATIVE.DST', 'SEMILEPTONIC.DST'],
#     #                 'visibilityFlag':[{'Visible': 'N', 'FileType': 'BHADRON.MDST'},
#     #                                   {'Visible': 'N', 'FileType': 'BHADRONCOMPLETEEVENT.DST'},
#     #                                   {'Visible': 'N', 'FileType': 'CHARM.MDST'},
#     #                                   {'Visible': 'N', 'FileType': 'CHARMCOMPLETEEVENT.DST'},
#     #                                   {'Visible': 'N', 'FileType': 'DIMUON.DST'},
#     #                                   {'Visible': 'N', 'FileType': 'EW.DST'},
#     #                                   {'Visible': 'N', 'FileType': 'FTAG.DST'},
#     #                                   {'Visible': 'N', 'FileType': 'LEPTONIC.MDST'},
#     #                                   {'Visible': 'N', 'FileType': 'MINIBIAS.DST'},
#     #                                   {'Visible': 'N', 'FileType': 'RADIATIVE.DST'},
#     #                                   {'Visible': 'N', 'FileType': 'SEMILEPTONIC.DST'}]}]

#     # prod = ProductionRequest()._buildProduction('Stripping', stepsInProd, {'BHADRON.MDST': 'Tier1-Buffer',
#     #                                                            'BHADRONCOMPLETEEVENT.DST': 'Tier1-Buffer',
#     #                                                            'CHARM.MDST': 'Tier1-Buffer',
#     #                                                            'CHARMCOMPLETEEVENT.DST': 'Tier1-Buffer',
#     #                                                            'DIMUON.DST': 'Tier1-Buffer',
#     #                                                            'EW.DST': 'Tier1-Buffer',
#     #                                                            'FTAG.DST': 'Tier1-Buffer',
#     #                                                            'LEPTONIC.MDST': 'Tier1-Buffer',
#     #                                                            'MINIBIAS.DST': 'Tier1-Buffer',
#     #                                                            'RADIATIVE.DST': 'Tier1-Buffer',
#     #                                                            'SEMILEPTONIC.DST': 'Tier1-Buffer'},
#     #                                 0, 100,
#     #                                 outputMode='Run', inputDataPolicy='protocol', inputDataList=lfns, events=500,
#     #                                 ancestorDepth=1)
#     # prod.LHCbJob._addParameter(prod.LHCbJob.workflow, 'runNumber', 'JDL', 167123, 'Input run number')
#     # prod.LHCbJob.setInputSandbox("pilot.cfg")
#     # prod.LHCbJob.setConfigArgs('pilot.cfg')
#     # res = DiracProduction().launchProduction(prod, False, True, 0)
#     assert res["OK"], res
