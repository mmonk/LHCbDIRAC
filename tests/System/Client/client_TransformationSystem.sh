#!/bin/bash
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# The purpose of this script is to test the most common plugins of replication/transformations:
#
# * ReplicateDataset
# * LHCbDSTBroadcast/LHCbMCDSTBroadcast
# * ArchiveDataset
# * DestroyDataset
# * ReduceReplicas/ReduceReplicasKeepDestination
# * RemoveDatasetFromDisk
# * RemoveReplicas
#
# Submitter should follow through the logs

echo "dirac-login lhcb_prmgr"
if ! dirac-login lhcb_prmgr; then
   exit 1
fi
echo " "


#Values to be used
stamptime=$(date +%Y%m%d_%H%M%S)
stime=$(date +"%H%M%S")
tdate=$(date +"20%y-%m-%d")
ttime=$(date +"%R")
version=${dirac-version}
mkdir -p TransformationSystemTest
directory=/lhcb/certification/Test/INIT/${version}/${tdate}/${stime}
#selecting a random USER Storage Element
#SEs=$(dirac-dms-show-se-status |grep USER |grep -v 'Banned\|Degraded\|-2' | awk '{print $1}')

SEs=$(dirac-dms-show-se-status |grep BUFFER |grep -v 'Banned\|Degraded\|-new' | awk '{print $1}')

x=0
for n in ${SEs}; do
  arrSE[x]=${n}
  let x++
done
# random=$[ $RANDOM % $x ]
# randomSE=${arrSE[$random]}

echo ""
echo "Submitting test production"
if ! python "${DIRAC}"/LHCbDIRAC/tests/System/Client/dirac-test-production.py -ddd; then
   exit 1
fi

transID=$(cat TransformationID)

# Create unique files and adding entry to the bkk"
echo ""
echo "Creating unique test files and adding entry to the bkk"
./client_Bookkeeping.sh --Files=5 --Name="Test_Transformation_System_" --Path="${PWD}"/TransformationSystemTest/

# Add the random files to the transformation
echo ""
echo "Adding files to Storage Element $randomSE"
# filesToUpload=$(ls TransformationSystemTest/)
# for file in $filesToUpload
# do
#   random=$[ $RANDOM % $x ]
#   randomSE=${arrSE[$random]}
#   echo "$directory/$file \
#        ./TransformationSystemTest/$file $randomSE" \
#        >> TransformationSystemTest/LFNlist.txt
# done

while IFS= read -r line; do
  random=$(( ${RANDOM} % $x ))
  randomSE=${arrSE[$random]}
  echo "${line} ${randomSE}"
done < LFNlist.txt >> ./LFNlistNew.txt

dirac-dms-add-file LFNlistNew.txt

LFNlist=$(cat LFNlist.txt | awk -vORS=, '{print $1}')

echo ""
echo "Adding the files to the test production: ${transID}"
if ! dirac-transformation-add-files "${transID}" --LFNs "${LFNlist}"; then
  exit 1
fi

# TODO: ___ Use Ramdom SEs___
