###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Option file for ganga jobs (taken from StarterKit)

    Needs input files to be supplied
"""
# pylint: skip-file

from GaudiConf import IOHelper
from Configurables import DaVinci, DecayTreeTuple
from DecayTreeTuple.Configuration import *

# Stream and stripping line we want to use
stream = "AllStreams"
line = "D2hhCompleteEventPromptDst2D2RSLine"

# Create an ntuple to capture D*+ decays from the StrippingLine line
dtt = DecayTreeTuple("TupleDstToD0pi_D0ToKpi")
dtt.Inputs = [f"/Event/{stream}/Phys/{line}/Particles"]
dtt.Decay = "[D*(2010)+ -> (D0 -> K- pi+) pi+]CC"

# Configure DaVinci
DaVinci().UserAlgorithms += [dtt]
DaVinci().InputType = "DST"
DaVinci().TupleFile = "DVntuple.root"
DaVinci().PrintFreq = 1000
DaVinci().DataType = "2012"
DaVinci().Simulation = True
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1
DaVinci().CondDBtag = "sim-20130522-1-vc-md100"
DaVinci().DDDBtag = "dddb-20130929-1"
