#!/bin/sh
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#-------------------------------------------------------------------------------

# Tests that require a $DIRACSCRIPTS pointing to DIRAC deployed scripts location,
# and a $DIRAC variable pointing to an installed DIRAC
# It also assumes that pilot.cfg contains all the necessary for running

echo -e "\n======> Test_dirac-jobexecLHCb.sh <======\n"

if [ ! -z "$DEBUG" ]
then
  echo '==> Running in DEBUG mode'
  DEBUG='-ddd'
else
  echo '==> Running in non-DEBUG mode'
fi

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo -e "THIS_DIR=${THIS_DIR}" |& tee -a clientTestOutputs.txt

# Creating the XML job description files
python "${THIS_DIR}/createJobXMLDescriptionsLHCb.py" $DEBUG

###############################################################################
# Running the real tests

# OK
echo -e "\n==> jobDescriptionLHCb-OK.xml"
dirac-jobexec jobDescriptionLHCb-OK.xml --cfg "${THIS_DIR}/../../../../DIRAC/tests/Integration/WorkloadManagementSystem/pilot.cfg" $DEBUG
ret_code=$?
if [ $ret_code -eq 0 ]
then
  echo -e "\nSuccess\n\n"
else
  echo -e "\nSomething wrong!"
  echo -e "ret_code = $ret_code \n\n"
  more jobDescriptionLHCb-OK.xml
  exit $ret_code
fi

# OK2
echo -e "\n==> jobDescriptionLHCb-multiSteps-OK.xml"
dirac-jobexec jobDescriptionLHCb-multiSteps-OK.xml --cfg "${THIS_DIR}/../../../../DIRAC/tests/Integration/WorkloadManagementSystem/pilot.cfg" $DEBUG
ret_code=$?
if [ $ret_code -eq 0 ]
then
  echo -e "\nSuccess\n\n"
else
  echo -e "\nSomething wrong!"
  echo -e "ret_code = $ret_code \n\n"
  more jobDescriptionLHCb-multiSteps-OK.xml
  exit $ret_code
fi


# # FAIL
echo -e "\n==> jobDescriptionLHCb-FAIL.xml"
dirac-jobexec jobDescriptionLHCb-FAIL.xml --cfg "${THIS_DIR}/../../../../DIRAC/tests/Integration/WorkloadManagementSystem/pilot.cfg" $DEBUG
ret_code=$?
# lb-run specific exit codes are no longer used to reschedule
if [ $ret_code -eq 111 ]
then
  echo -e "\nSuccess\n\n"
else
  echo -e "\nSomething wrong!"
  echo -e "ret_code = $ret_code \n\n"
  more jobDescriptionLHCb-FAIL.xml
  exit $ret_code
fi

# # FAIL2
echo -e "\n==> jobDescriptionLHCb-multiSteps-FAIL.xml"
dirac-jobexec jobDescriptionLHCb-multiSteps-FAIL.xml --cfg "${THIS_DIR}/../../../../DIRAC/tests/Integration/WorkloadManagementSystem/pilot.cfg" $DEBUG
ret_code=$?
# lb-run specific exit codes are no longer used to reschedule
if [ $ret_code -eq 111 ]
then
  echo -e "\nSuccess\n\n"
else
  echo -e "\nSomething wrong!\n\n"
  echo -e "ret_code = $ret_code \n\n"
  more jobDescriptionLHCb-multiSteps-FAIL.xml
  exit $ret_code
fi


# FAIL with exit code > 255
echo -e "\n==> jobDescriptionLHCb-FAIL1502.xml"
dirac-jobexec jobDescriptionLHCb-FAIL1502.xml --cfg "${THIS_DIR}/../../../../DIRAC/tests/Integration/WorkloadManagementSystem/pilot.cfg" $DEBUG
ret_code=$?
if [ $ret_code -eq 222 ] # This is 1502 & 255 (0xDE)
then
  echo -e "\nSuccess\n\n"
else
  echo -e "\nSomething wrong!"
  echo -e "ret_code = $ret_code \n\n"
  more jobDescriptionLHCb-FAIL1502.xml
  exit $ret_code
fi

# # Removals
rm jobDescriptionLHCb-OK.xml
rm jobDescriptionLHCb-multiSteps-OK.xml
rm jobDescriptionLHCb-FAIL.xml
rm jobDescriptionLHCb-multiSteps-FAIL.xml
rm jobDescriptionLHCb-FAIL1502.xml
rm Script1_CodeOutput.log
